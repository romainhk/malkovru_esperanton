package epo.foss.romain.malkovruesperanton;

import android.content.Context;
import android.database.SQLException;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Random;

public class WordListAdapter extends RecyclerView.Adapter<WordListAdapter.ViewHolder> {
    private ArrayList<Word> mDataset;
    static DBHelper db;
    public static final int LIST_SIZE = 10;

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public TextView wordView;
        public TextView translationView;
        public ViewHolder(View v) {
            super(v);
            wordView = (TextView) itemView.findViewById(R.id.word);
            translationView = (TextView) itemView.findViewById(R.id.translation);
        }
    }

    public WordListAdapter(Context context) {
        db = new DBHelper(context);
        /*try {
            db.createDataBase();
        } catch (Error ioe) {
            throw new Error("Unable to create database");
        }

        try {
            db.openDataBase();
        }catch(SQLException sqle){
            throw sqle;
        }*/
        this.generateWordList(WordListAdapter.LIST_SIZE);
    }

    // Create new views (invoked by the layout manager)
    @Override
    public WordListAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                   int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.item_word, parent, false);

        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.wordView.setText(mDataset.get(position).getEpo());
        holder.translationView.setText(mDataset.get(position).getFr());
    }

    @Override
    public int getItemCount() {
        return mDataset.size();
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    public void generateWordList(int size) {
        ArrayList<Word> list = new ArrayList<>();
        ArrayList<Integer> ids = db.getIds();
        Random r = new Random();

        while (list.size() < size) {
            int id_ = r.nextInt();
            if (!ids.contains(id_)) {
                Word item = db.getData(id_);
                list.add(item);
                ids.add(id_);
            }
        }

        mDataset = list;
    }
}
