package epo.foss.romain.malkovruesperanton;

/**
 * Word in esperanto and its translation.
 */
public class Word {

    private String epo;
    private String fr;

    public Word(String epo, String fr) {
        this.epo = epo;
        this.fr = fr;
    }

    public String getEpo() {
        return this.epo;
    }

    public String getFr() {
        return this.fr;
    }

}
