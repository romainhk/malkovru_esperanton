package epo.foss.romain.malkovruesperanton;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import java.util.ArrayList;

/**
 * DBHelper
 * @author Romain Hennuyer
 * @version 1
 **/
public class DBHelper extends SQLiteOpenHelper {
    private final static int DB_VERSION = 1;

    public static final String DB_NAME = "db.sqlite";
    //private static String DB_PATH = "";
    private SQLiteDatabase database;
    //private final Context context;

    public static final String DICT_TABLE_NAME = "Dictionary";
    public static final String DICT_COLUMN_ID = "id";
    public static final String DICT_COLUMN_EPO = "epo";
    public static final String DICT_COLUMN_FR = "fr";
    //private static final String SQL_CREATE_ENTRIES = "CREATE TABLE " + MESSAGES_TABLE_NAME +
    //        "(id integer primary key, epo text, fr text)";

    @Override
    public void onCreate(SQLiteDatabase db) {
        //db.execSQL(SQL_CREATE_ENTRIES);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        /*Log.d(TAG, "Updating database : "+oldVersion+" -> "+newVersion);
        db.execSQL("DELETE FROM " + MESSAGES_TABLE_NAME);
        db.execSQL("DROP TABLE IF EXISTS " + MESSAGES_TABLE_NAME);
        onCreate(db);*/
    }

    public Word getData(int id) {
        Cursor cursor = database.rawQuery("SELECT * FROM " + DICT_TABLE_NAME + " WHERE id=" + id + "", null);

        String epo = cursor.getString(cursor.getColumnIndex(DICT_COLUMN_EPO));
        String fr = cursor.getString(cursor.getColumnIndex(DICT_COLUMN_FR));
        Word word = new Word(epo, fr);
        return word;
    }

    public ArrayList<Integer> getIds() {
        ArrayList<Integer> list = new ArrayList<>();
        Cursor cursor = database.rawQuery("SELECT id FROM " + DICT_TABLE_NAME, null);

        if(cursor.moveToFirst()){
            do {
                list.add(cursor.getInt(cursor.getColumnIndex(DICT_COLUMN_ID)));
            } while (cursor.moveToNext());
        }
        cursor.close();
        return list;
    }

    public DBHelper(Context context)
    {
        super(context, DB_NAME, null, DB_VERSION);
        //this.context = context;
        this.database = getReadableDatabase();
        //DB_PATH = context.getApplicationInfo().dataDir + "/databases/";
    }
/*
    public void createDataBase() throws Error
    {
        boolean mDataBaseExist = checkDataBase();
        if(!mDataBaseExist)
        {
            this.getReadableDatabase();
            this.close();
            try
            {
                copyDataBase();
                Log.e(TAG, "createDatabase database created");
            }
            catch (IOException mIOException)
            {
                throw new Error("ErrorCopyingDataBase");
            }
        }
    }

    private boolean checkDataBase()
    {
        File dbFile = new File(DB_PATH + DB_NAME);
        //Log.v("dbFile", dbFile + "   "+ dbFile.exists());
        return dbFile.exists();
    }

    //Copy the database from assets
    private void copyDataBase() throws IOException
    {
        InputStream mInput = context.getAssets().open(DB_NAME);
        String outFileName = DB_PATH + DB_NAME;
        OutputStream mOutput = new FileOutputStream(outFileName);
        byte[] mBuffer = new byte[1024];
        int mLength;
        while ((mLength = mInput.read(mBuffer))>0)
        {
            mOutput.write(mBuffer, 0, mLength);
        }
        mOutput.flush();
        mOutput.close();
        mInput.close();
    }

    public boolean openDataBase() throws SQLException
    {
        String mPath = DB_PATH + DB_NAME;
        //Log.v("mPath", mPath);
        database = SQLiteDatabase.openDatabase(mPath, null, SQLiteDatabase.OPEN_READONLY);
        return database != null;
    }

    @Override
    public synchronized void close()
    {
        if(database != null)
            database.close();
        super.close();
    }
    */
}